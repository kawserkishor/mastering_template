<title>BlackPod</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="{{asset('css/style.css')}}" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="{{asset('css/coin-slider.css')}}" />
<script type="text/javascript" src="{{asset('js/cufon-yui.js')}}"></script>
<script type="text/javascript" src="{{asset('js/cufon-aller.js')}}"></script>
<script type="text/javascript" src="{{asset('js/jquery-1.4.2.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/script.js')}}"></script>
<script type="text/javascript" src="{{asset('js/coin-slider.min.js')}}"></script>