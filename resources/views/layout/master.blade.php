
<!DOCTYPE html>
<html>
<head>
   @include('partials.head')
</head>
<body>
<div class="main">
    <div class="header">
        <div class="header_resize">
            <div class="logo">
                <h1><a href="{{url('/')}}">black<span>pod</span> <small>Company Slogan Goes Here</small></a></h1>
            </div>
           <!-- nav -->
            <div class="menu_nav">
                @include('partials.nav')
            </div>
           <!-- nav -->
            <div class="clr"></div>
            <!-- slider -->
            @include('partials.slider')
            <!-- slider -->
            <div class="clr"></div>
        </div>
    </div>
        <!-- main content -->
            @yield('content')
        <!-- main content -->
    <div class="footer">
       @include('partials.footer')
    </div>
</div>
<div align=center>This template  downloaded form <a href='http://all-free-download.com/free-website-templates/'>free website templates</a></div></body>
</html>
